/*
 * Arquivo principal do projeto
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  11/01/2021
*/

#include "./headers/Sistema.h"
#include <fstream>
#include <vector>
#include <iostream>
#include <regex>
#include <time.h>
#include <set>
#include <chrono>
#include <iomanip>

Sistema::Sistema(int x)
{
    this->tam = x;
    this->tamRandomizadas = 0;
    arqSaidaModuloTeste = new ofstream("./RelatorioModuloTeste.txt");
}

Sistema::~Sistema()
{
    delete this->arqSaidaModuloTeste;
    excluirRandomizada();
}

void Sistema::lerAquivo(string nomeArq)
{
    auto start = std::chrono::steady_clock::now();
    ifstream arq(nomeArq);

    if (arq.is_open())
    {

        cout << "Abrindo Arquivo" << endl;

        string str;
        string result;
        string teste[6];
        int count = 0;
        int posicao = 0;

        getline(arq, str); //ignora primeira linha

        while (getline(arq, str))
        {

            posicao = 0;
            std::stringstream ssin(str);

            // quebrando a linha linha por , e adicionando ao vetor
            while (std::getline(ssin, result, ','))
            {
                teste[posicao] = result;
                posicao++;
            }

            // criando o dado, adicionando ao vetor e count++
            Dado *aux = new Dado(teste[0], teste[1], teste[2], std::stoi(teste[3]), std::stoi(teste[4]), std::stoi(teste[5]));
            instancias[count] = aux;
            count++;
        }
        cout << "Fim da Leitura" << endl
             << "Quantidade de instancias: " << count << endl;
    }
    else
        cerr << "ERRO: O arquivo nao pode ser aberto!" << endl;

    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> tempo = end - start;
    std::cout << "*** Tempo de Processamento da leitura e criacao de dados = " << tempo.count() << "s" << std::endl;
}

//função merge pra intercalação
void Sistema::merge(Dado **vec, Dado **aux, int inicio, int meio, int fim)
{
    int i, j, k;
    i = inicio;
    j = meio;
    k = 0;

    while (i < meio && j < fim)
    {
        if (vec[i]->getEstado() < vec[j]->getEstado()) // verifica se o estado é menor
        {
            aux[k] = vec[i];
            i++;
        }
        else if (vec[i]->getEstado() > vec[j]->getEstado()) // verifica se o estado é maior
        {
            aux[k] = vec[j];
            j++;
        }
        else // só vem se o estado for igual - testar cidade agora
        {
            if (vec[i]->getCidade() < vec[j]->getCidade()) // verifica se a cidade é menor
            {
                aux[k] = vec[i];
                i++;
            }
            else if (vec[i]->getCidade() > vec[j]->getCidade()) // verifica se a cidade é maior
            {
                aux[k] = vec[j];
                j++;
            }
            else
            { // é aqui q vai comparar por data se city ==
                if (vec[i]->getData() < vec[j]->getData()) // se a data não for menor é necessariamente maior
                {
                    aux[k] = vec[i];
                    i++;
                }
                else // maior, pois não temos datas iguais no arquivo pra mesma cidade
                {
                    aux[k] = vec[j];
                    j++;
                }
            }
        }
        k++;
    }

    while (i < meio)
    {
        aux[k] = vec[i];
        i++;
        k++;
    }

    while (j < fim)
    {
        aux[k] = vec[j];
        j++;
        k++;
    }

    for (int p = inicio; p < fim; p++)
        vec[p] = aux[p - inicio];
}

//função recursiva
void Sistema::mergeSortAux(Dado **vec, Dado **aux, int inicio, int fim)
{
    //cout << inicio << " " << fim << endl;
    if (inicio < fim - 1)
    {
        int meio = (inicio + fim) / 2;
        mergeSortAux(vec, aux, inicio, meio);
        mergeSortAux(vec, aux, meio, fim);
        merge(vec, aux, inicio, meio, fim);
    }
    //cout << "Terminou o MergeSortAux"<< endl;
}

// funcao principal que chama mergeSortAux
void Sistema::mergeSort()
{
    auto start = std::chrono::steady_clock::now();

    cout << "Iniciando Ordernacao pelo MergeSort" << endl;
    Dado **vecAux = new Dado *[tam];
    this->mergeSortAux(instancias, vecAux, 0, this->tam);
    delete[] vecAux;

    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> tempo = end - start;
    std::cout << "*** Tempo de Processamento Pre-processamento do MergeSort = " << tempo.count() << "s" << std::endl;
}

//funcao para gerar arquivo processado
void Sistema::escreveArquivoSaida()
{

    cout << "Iniciando escrita no arquivo de saida" << endl;
    ofstream arq("./brazil_covid19_cities_processado.txt");

    //ofstream arq("arqTexto.txt");

    arq << "state,name,date,code,cases,deaths" << endl;

    for (int i = 0; i < this->tam; i++)
    {
        arq << instancias[i]->getEstado() << "," << instancias[i]->getCidade() << ","
            << instancias[i]->getData() << "," << instancias[i]->getCodCidade() << ","
            << instancias[i]->getCasos() << "," << instancias[i]->getObitos() << endl;
    }

    arq.close();
}

//funcao para transformcar total acumulado em total diario
void Sistema::TransformaAcumuladoDiario()
{
    int casosNoDia = 0;
    int casosDiaAnterior;
    int casosObitosNoDia = 0;
    int casosObtitosDiaAnterior;

    //cout << "Iniciando transformacao casos acumulado em diarios" << endl;

    for (int i = 0; i < 5570; i++) //557 cidades
    {
        // primeiro dia
       
        int inicio = 257*i;
        int parada = 257*(i+1);

        if(inicio % 257 == 0) // 257 datas
        {   
            //pega o primeiro dia como referencia
            casosDiaAnterior = instancias[inicio]->getCasos();
            casosObtitosDiaAnterior = instancias[inicio]->getObitos();
            instancias[inicio]->setCasos(instancias[inicio]->getCasos() ); 
            instancias[inicio]->setObitos(instancias[inicio]->getObitos());
        }        
        
        for(int j=inicio+1; j < parada; j++){
            //CASOS
            casosNoDia = instancias[j]->getCasos() - casosDiaAnterior;
            casosDiaAnterior += casosNoDia; // acumulado
            instancias[j]->setCasos(casosNoDia);
        
            // OBITOS
            casosObitosNoDia = instancias[j]->getObitos() - casosObtitosDiaAnterior;
            casosObtitosDiaAnterior += casosObitosNoDia; // acumulado
            instancias[j]->setObitos(casosObitosNoDia);
        }
    }
}

//funcao para realizar leitura randomizada do arquivo processado
void Sistema::leituraRandomizada(int N)
{
    //realizar a leitura randomizada de N registro do arquivo ./brazil_covid19_cities_processado.csv
    //cout << "Iniciando Leitura Randomizada" << endl;
    //cout << "RAND Maximo: " << RAND_MAX << endl;
    excluirRandomizada();
    //cout << "t*" << endl;
    ifstream arq("./brazil_covid19_cities_processado.txt");
    //cout << "t**" << endl;

    if (arq.is_open())
    {
        //cout << "t***" << endl;
        //cout << "Abrindo Arquivo Processado" << endl;     
        srand(time(NULL));
        set<int> listaLinhas;        
        string str;
        string result;
        string teste[6];
        int count = 0;
        int posicao = 0; 
        int posicaoUsada = 0;   

        this->instanciasRandomizadas = new Dado *[N];
        this->tamRandomizadas = N;

        //cout << "t1" << endl;
        for(int i=0;i<this->tamRandomizadas;i++){
            //cout << i;
            int a = rand() % this->tam;
            int num = (28741*(rand()%31)) + (3587*(rand()%19)) + (43871*(rand()%7)) - (43587*(rand()%5)) + (92825*(rand()%3)) + (32587*(rand()%2))+ a;
            //funçãoo pra gerar numero da linha aleatorio devido ao RAND_MAX no pc


            if(num > this->tam) // verifica se o numero não é maior q a qnt de linhas do arquivo e corrigi
                num %= this->tam;
            
            if(num <1  || listaLinhas.count(num)){ //sem numero <=0 ou já selecionado
                i--;
            }else{         
                listaLinhas.insert(num); // insere no set
            }
        }


        //cout << "t2" << endl;
        getline(arq, str); //ignora primeira linha

        while (posicaoUsada < N && getline(arq, str)) // lê até ter o tanto de instancias desejados
        {

            if(listaLinhas.count(count) == 1){ // só considera a linha se for uma linha selecionada

                
                //cout << count << " ";
                posicao = 0;
                std::stringstream ssin(str);

                // quebrando a linha linha por , e adicionando ao vetor
                while (std::getline(ssin, result, ','))
                {
                    teste[posicao] = result;
                    posicao++;
                }

                // criando o dado, adicionando ao vetor e count++
                //arq << "state,name,date,code,cases,deaths" << endl;
                Dado *aux = new Dado(teste[2], teste[0], teste[1], std::stoi(teste[3]), std::stoi(teste[4]), std::stoi(teste[5]));
                instanciasRandomizadas[posicaoUsada] = aux;
                posicaoUsada++;
                //cout << count << endl;
                //cout << posicaoUsada << " ";
            }
            count++;
        }
        cout << "Carregamento dos registros realizada" << endl;
    }
    else
        cerr << "ERRO: O arquivo nao pode ser aberto!" << endl;
}

//funcao para excluir instancias randomizadas
void Sistema::excluirRandomizada()
{
    //EXCLUINDO INSTANCIAS LIDAS
    //cout << "excluir" << tamRandomizadas << endl;
    for (int i = 0; i < this->tamRandomizadas; i++)
        delete instanciasRandomizadas[i];
    //cout << "excluir2" << endl;
    delete[] this->instanciasRandomizadas;

    this->tamRandomizadas = 0;
}

//funcao para gerar arquivo de saida op3 modulo teste ou printar na tela dependendo do N utilizado
void Sistema::gerarArquivoSaidaModuloTeste(string nomeAlgoritmo)
{

    if(nomeAlgoritmo == ""){  // limpa arquivo se for testar novamente sem fechar o programar
        delete arqSaidaModuloTeste;   
        arqSaidaModuloTeste = new ofstream("./RelatorioModuloTeste.txt");
        return;
    }

    if(this->tamRandomizadas < 11){ // MENOR Q 11, SAIDA NO CONSOLE
        cout << "** ALGORITMO " << nomeAlgoritmo << endl;
        imprimirRandomizado();
        return;
    }

    *arqSaidaModuloTeste << "** ALGORITMO " << nomeAlgoritmo << " **" << endl;

    *arqSaidaModuloTeste << "cases,deaths,state,name,date,code" << endl;

    for (int i = 0; i < this->tamRandomizadas; i++)
    {
        *arqSaidaModuloTeste << instanciasRandomizadas[i]->getCasos() << "," << instanciasRandomizadas[i]->getObitos() << ","
            << instanciasRandomizadas[i]->getEstado() << "," << instanciasRandomizadas[i]->getCidade() << ","
            << instanciasRandomizadas[i]->getData() << "," << instanciasRandomizadas[i]->getCodCidade() << endl;
    }

    *arqSaidaModuloTeste << endl;

    //cout << "Gerando do Arquivo finalizada" << endl;

    //*arqSaidaModuloTeste->close();
}

//** RODRIGO **//
// implementação do algoritmo ShellSort
void Sistema::shellSort()
{
    Dado **vec = this->instanciasRandomizadas; // referencia pra variavel de sistema
    int size = this->tamRandomizadas;          // referencia pra variavel de sistema

    int i, j, h, trocas = 0, comp = 0;
    Dado *aux;
    h = size / 2;

    while (h > 0)
    {
        for (i = h; i < size; i++)
        {
            aux = vec[i];

            for (j = i - h; (j >= 0) && (vec[j]->getCasos() > aux->getCasos()); j = j - h)
            {
                vec[j + h] = vec[j];
                trocas++; //trocas fica aqui, pois ele sempre pelo vec[j+h]=aux, e este comando nem
                          //sempre eh uma troca de fato, pois, por vezes ele so copia os valor da posicao
                          //para ela mesma
                comp++; // lugar correto, pq ocorre mais de uma comparação aqui
            }

            vec[j + h] = aux; //so completa a troca ou copia o valor de um indice para ele mesmo, caso o for seja falso
            trocas++; // outra troca
        }

        h = h / 2;
    }

    this->metrica[0] = comp; // salva a metrica da iteração
    this->metrica[1] = trocas; // salva a metrica da iteração

}

//** LUCCA **//
//chamda do quickSort pública
void Sistema::quickSort()
{
    int qntComp = 0; // Conta quantidade de comparações
    int qntKey = 0;  // Conta quantidade de movimentações de chaves
    this->quickSortAux(instanciasRandomizadas, 0, tamRandomizadas - 1, &qntComp, &qntKey);
    this->metrica[0] = qntComp; // salva a metrica da iteração
    this->metrica[1] = qntKey; // salva a metrica da iteração
}

// função recursiva QuickSort
void Sistema::quickSortAux(Dado **vec, int inicio, int fim, int *qntComp, int *qntKey)
{
    if (inicio - fim < 0)
    {
        (*qntComp)++;
        int q = this->particionamento(vec, inicio, fim, qntComp, qntKey);
        this->quickSortAux(vec, inicio, q, qntComp, qntKey);
        this->quickSortAux(vec, q + 1, fim, qntComp, qntKey);
    }
}

// função efetiva de ordenação por particionamento
int Sistema::particionamento(Dado **vec, int inicio, int fim, int *qntComp, int *qntKey)
{
    int pivo;
    int i = inicio;
    int j = fim;
    int a = vec[inicio + 1]->getCasos(), b = vec[(inicio + fim + 1) / 2]->getCasos(), c = vec[fim]->getCasos();

    if (a > b && a > c)
    {
        pivo = b > c ? b : c;
    }
    else if (b > a && b > c)
    {
        pivo = a > c ? a : c;
    }
    else
    {
        pivo = a > b ? a : b;
    }

    do
    {

        while (vec[i]->getCasos() < pivo)
        {
            i++;
            (*qntComp)++;
        }

        while (vec[j]->getCasos() > pivo)
        {
            j--;
            (*qntComp)++;
        }

        if (i <= j)
        {
            Dado *aux = vec[i];
            vec[i] = vec[j];
            vec[j] = aux;
            i++, j--;
            (*qntKey)++;
        }

    } while (i <= j);

    return j;
}

//** ESTER **//
//Algoritmo de intercalação computando o número de comparações e movimentações | chave: numero de casos confirmados
void Sistema::mergeCasos(Dado **vec, Dado **aux, int inicio, int meio, int fim, int *comp, int *mov)
{
    int i, j, k;
    i = inicio;
    j = meio;
    k = 0;

    while (i < meio && j < fim)
    {

        *comp = *comp + 1;
        if (vec[i]->getCasos() < vec[j]->getCasos())
        {
            aux[k] = vec[i];
            i++;
        }
        else
        {
            aux[k] = vec[j];
            j++;
            *mov = *mov + 1;
        }
        k++;
    }

    while (i < meio)
    {
        aux[k] = vec[i];
        i++;
        k++;
    }

    while (j < fim)
    {
        aux[k] = vec[j];
        j++;
        k++;
    }

    for (int p = inicio; p < fim; p++)
        vec[p] = aux[p - inicio];
}

//Função auxliar recursiva que computa as comparações e movimentações
void Sistema::mergeSortAuxRand(Dado **vec, Dado **aux, int inicio, int fim, int *comp, int *mov)
{

    if (inicio < fim - 1)
    {
        
        int meio = (inicio + fim) / 2;
        mergeSortAuxRand(vec, aux, inicio, meio, comp, mov);
        mergeSortAuxRand(vec, aux, meio, fim, comp, mov);
        mergeCasos(vec, aux, inicio, meio, fim, comp, mov);
    }
}

//Ordena um conjunto de N dados aleatorios
void Sistema::mergeSortRand()
{

    int N = this->tamRandomizadas;
    int c = 0, m = 0;
        Dado **vecAux = new Dado *[N];
        this->mergeSortAuxRand(instanciasRandomizadas, vecAux, 0, N, &c, &m);
        delete[] vecAux;
    this->metrica[0] = c; // salva a metrica da iteração
    this->metrica[1] = m; // salva a metrica da iteração
}

//imprime no console as instancias randomizadas
void Sistema::imprimirRandomizado(){
    cout << "cases,deaths,state,name,date,code" << endl;

    for (int i = 0; i < this->tamRandomizadas; i++)
    {
        cout << instanciasRandomizadas[i]->getCasos() << "," << instanciasRandomizadas[i]->getObitos() << ","
            << instanciasRandomizadas[i]->getEstado() << "," << instanciasRandomizadas[i]->getCidade() << ","
            << instanciasRandomizadas[i]->getData() << "," << instanciasRandomizadas[i]->getCodCidade() << endl;
    }
    cout << endl;
}

//funcao para testar os algoritmos e métricas de desempenho e gerar o arquivo saida.txt
void Sistema::testarAlgoOrdenacao(){
    // teste algoritmo de ordenação
    //vec com valores de N para geração randomizada
    int vec[] = {10000,50000,100000,500000,1000000}; // tamanhos de instancias desejados
    int tam = 5; // define o tamanho do vetor, se quiser alterar a qnt dos valores de n
    int m = 10; // m = quantidade de repetição para geração da médias da métrica de cada algoritmo
    // ADOTADO M = 2 PARA TESTES RAPIDOS
    double totalqntComp=0, totalqntKey=0;
    chrono::duration<double> totaltempo = totaltempo.zero();
    auto start = chrono::steady_clock::now();
    auto end = chrono::steady_clock::now();
    chrono::duration<double> tempo;

    arqSaida = new ofstream("./saida.txt");

    *arqSaida << "*** Relatório para comparações estatísticas *** " << endl;

    if(this->tamRandomizadas == 0){
        cout << "Eh necessario gerar instancias randomizadas primeiramente!" << endl;
    }else{
        this->tamRandomizadas = 0;
        //chamar os algoritmos, gravar métricas, repetir x vezes, gerar dados pro arquivo texto
        for(int i=0; i < tam; i++){ // para cada um dos valores de n, vamos testar o(s) algoritmos
            
            *arqSaida << "*** Tamanho Amostra: " << vec[i] << endl;
            *arqSaida << "** Algoritmo ShellSort" << endl;
            //    cout << "ENTRANDO2" << endl;
            for(int j=0; j < m; j++){ // garante a repetição de cada algoritmo m vezes
                    leituraRandomizada(vec[i]);//gerando vec[i] instancias aleatorias
                    start = chrono::steady_clock::now(); // inicia contagem tempo
                    shellSort(); //chama o algoritmo desejado
                    auto end = chrono::steady_clock::now(); // termina contagem tempo
                    tempo = end - start; // calcula duração do algoritmo
                    totalqntComp+=this->metrica[0]; //salva total de comp
                    totalqntKey+=this->metrica[1]; // salvo total de movkey
                    totaltempo+=tempo;// computar somatorio (ver onde vamos gravar as métricas)
                    *arqSaida << fixed << setprecision(2) << "  Iteracao " << j << ": Qnt Comp: " << this->metrica[0] <<
                    " Qnt MovKey: " << this->metrica[1] << " Tempo: " << setprecision(6) << tempo.count() << endl;
            }

            // calcular média das métricas e gravar no arquivo function escreveArquivoSaidaComparacao(str)
            *arqSaida << fixed << setprecision(2) << "  #Resumo|Medias: " << "Comp: " << totalqntComp/m << " MovKey: " << totalqntKey/m
            << " Media Tempo: " << setprecision(6) << totaltempo.count()/m << endl << endl;
            totalqntComp=0, totalqntKey=0; // zera os parametros de comparação
            totaltempo = totaltempo.zero(); //= std::chrono::milliseconds::zero();

            //*** OUTRO ALGORITMO COM M INTERACOES***//
            //passar pro proximo algoritmo com a mesma quantidade vec[i] de instancias
                        *arqSaida << "** Algoritmo QuickSort" << endl;
            //    cout << "ENTRANDO2" << endl;
            for(int j=0; j < m; j++){ // garante a repetição de cada algoritmo m vezes
                    leituraRandomizada(vec[i]);//gerando vec[i] instancias aleatorias
                    start = chrono::steady_clock::now(); // inicia contagem tempo
                    quickSort(); //chama o algoritmo desejado
                    auto end = chrono::steady_clock::now(); // termina contagem tempo
                    tempo = end - start; // calcula duração do algoritmo
                    totalqntComp+=this->metrica[0]; //salva total de comp
                    totalqntKey+=this->metrica[1]; // salvo total de movkey
                    totaltempo+=tempo;// computar somatorio (ver onde vamos gravar as métricas)
                    *arqSaida << fixed << setprecision(2) << "  Iteracao " << j << ": Qnt Comp: " << this->metrica[0] <<
                    " Qnt MovKey: " << this->metrica[1] << " Tempo: " << setprecision(6) << tempo.count() << endl;
            }

            // calcular média das métricas e gravar no arquivo function escreveArquivoSaidaComparacao(str)
            *arqSaida << fixed << setprecision(2) << "  #Resumo|Medias: " << "Comp: " << totalqntComp/m << " MovKey: " << totalqntKey/m
            << " Media Tempo: " << setprecision(6) << totaltempo.count()/m << endl << endl;
            totalqntComp=0, totalqntKey=0; // zera os parametros de comparação
            totaltempo = totaltempo.zero(); //= std::chrono::milliseconds::zero();

            //*** OUTRO ALGORITMO COM M INTERACOES***//
            *arqSaida << "** Algoritmo MergeSort" << endl;
            //    cout << "ENTRANDO2" << endl;
            for(int j=0; j < m; j++){ // garante a repetição de cada algoritmo m vezes
                    leituraRandomizada(vec[i]);//gerando vec[i] instancias aleatorias
                    start = chrono::steady_clock::now(); // inicia contagem tempo
                    mergeSortRand(); //chama o algoritmo desejado
                    auto end = chrono::steady_clock::now(); // termina contagem tempo
                    tempo = end - start; // calcula duração do algoritmo
                    totalqntComp+=this->metrica[0]; //salva total de comp
                    totalqntKey+=this->metrica[1]; // salvo total de movkey
                    totaltempo+=tempo;// computar somatorio (ver onde vamos gravar as métricas)
                    *arqSaida << fixed << setprecision(2) << "  Iteracao " << j << ": Qnt Comp: " << this->metrica[0] <<
                    " Qnt MovKey: " << this->metrica[1] << " Tempo: " << setprecision(6) << tempo.count() << endl;
            }

            // calcular média das métricas e gravar no arquivo function escreveArquivoSaidaComparacao(str)
            *arqSaida << fixed << setprecision(2) << "  #Resumo|Medias: " << "Comp: " << totalqntComp/m << " MovKey: " << totalqntKey/m
            << " Media Tempo: " << setprecision(6) << totaltempo.count()/m << endl << endl;
            totalqntComp=0, totalqntKey=0; // zera os parametros de comparação
            totaltempo = totaltempo.zero(); //= std::chrono::milliseconds::zero();

        }
        cout << "Geracao de Arquivo de Comparacao estatistico finalizado" << endl;
    }

    delete arqSaida;

}

//funcao para testar os algoritmos durante o modulo de teste
void Sistema::testarAlgoOrdenacaoModuloTeste(){
    // teste algoritmo de ordenação modulo teste
    // não precisa de parametro pois assume-se que tem q gerar as instancias randomizadas primeiro
    //if se tam <> alguma coisa
    if(this->tamRandomizadas == 0){
        cout << "Eh necessario gerar instancias randomizadas primeiramente!" << endl;
    }else{
        //chamar algoritmos de ordenacao e printar na tela ou gravar no arquivo
        gerarArquivoSaidaModuloTeste("");
        shellSort();
        gerarArquivoSaidaModuloTeste("ShellSort"); // verificar pois acho q vai sobreescrever, criar ponteiro
        quickSort();
        gerarArquivoSaidaModuloTeste("QuickSort");
        mergeSortRand();
        gerarArquivoSaidaModuloTeste("MergeSort");
        //delete arqSaidaModuloTeste;

        cout << "Teste de Algortimos de Ordenacao Finalizado" << endl;
    }

}

//funcao pra fazer o pre processamento do arquivo no modulo teste
void Sistema::ProcessarModuloTeste(string entrada){
    this->instancias = new Dado *[tam]; // criando vetor pra função
    
    lerAquivo(entrada);    
    mergeSort();    
    TransformaAcumuladoDiario();
    escreveArquivoSaida();
    
    //deletando as instancias, pois não serão mais necessarias
    //em tese nem precisaria das instancias randomizadas, poderia utilizar a mesma variável
    //acho q não convem mudar pelo trabalho que vai ser e pq o codigo fica mais claro
    for (int i = 0; i < tam; i++)
        delete instancias[i];

    delete[] this->instancias;
    cout << "Pre-processamento e geracao arquivo processados finalizados" << endl;
}
