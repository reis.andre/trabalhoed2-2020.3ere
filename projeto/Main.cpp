/*
 * Arquivo principal do projeto
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  10/01/2021
*/
#include <time.h>
#include <chrono>
#include "./headers/Sistema.h"
#include <iomanip>

int main(int argc, char *argv[])
{

    //cout << "Iniciando main" << endl; 

    srand(time(NULL)); //seta a semente de tempo para ser aleatorio

    // Verifica se atende o mínimo de parâmetros
    if (argc != 2)
    {
        cout << "E necessario passar o nome de um arquivo de entrada" << endl;
        exit(1);
    }

    const char *entrada = argv[1]; 

    cout << "* * * MODULO DE TESTE * * *" << endl;;
    cout << "Arquivo de entrada: " << entrada << endl;

    Sistema* sistema = new Sistema(1431490); // passando o tamanho do vetor por parametro

    //cout << fixed << setprecision(2);
    //cout << 145634565.87 << endl;
    //sistema->leituraRandomizada(50);
    //sistema->testarAlgoOrdenacaoModuloTeste(); // modulo de teste rapido
    //sistema->testarAlgoOrdenacao(); // arquivo saida
    //return 0;
 
    //*** CRIAR MODO DE TESTE E LEITURA DOS PARAMETROS ATRAVES DA FUNÇÃO MAIN ***/
   
    int op, qnt;
    bool t = false;  /// COMEÇA SEMPRE COM FALSO
                    ///controle para realização do preprocessamento, obrigatorio para realizar outras operacoes

    cout << "1- Pre-processamento dos dados\n2- Importacao de N registros aleatorios"
         << "\n3- Cada um dos algoritmos de ordenacao com N Registros\n4- Sair" << endl;
    cout << "Escolha uma opcao: ";
    cin >> op;

    while(op != 4){ // fica no menu até digitar 4
        switch (op)
        {
        case 1:
            sistema->ProcessarModuloTeste(entrada);
            t = true;
            break;
        case 2:
            if(t){
                cout << "Escolha a quantidade de registros: "; cin >> qnt;
                if(qnt < 1 || qnt > 1400000)
                    cout << "DIgite uma quantidade valida (0, 1.4kk]. Aperte 2 para refazer" << endl;
                else
                    sistema->leituraRandomizada(qnt);
            }else
                cout << "Necessario realizar o pre-processamento primeiro" << endl;
            break;
        case 3:
            if(t){
                sistema->testarAlgoOrdenacaoModuloTeste(); // sempre continua escrevendo, dar um cheiro de sobreecreever
            }else
                cout << "Necessario realizar o pre-processamento primeiro" << endl;
            break;
        default:
            cout << "Opcao invalida!" <<endl;
            break;
        }
        cout << endl <<"1- Pre-processamento dos dados\n2- Importacao de N registros aleatorios"
         << "\n3- Cada um dos algoritmos de ordenacao com N Registros\n4- Sair" << endl;
        cout << "Escolha uma opcao: ";  
        cin >> op;
    }
    
    delete sistema;
    cout << "* * * ENCERRANDO O PROGRAMA * * *" << endl;;
 
    return 0;
}