/*
 * Arquivo principal do projeto
 * 
 * Professor: Marcelo Caniato Renhe
 *
 * Autores: André Luiz dos Reis - 201965004A
 *          Ester Goulart Cruz Rodrigues - 201965028AC
 *          Lucca Oliveira Schrôder - 201765205AC
 *          Rodrigo da Silva Soares - 201765218AB
 * Data de Criação:  11/01/2021
*/

#ifndef SISTEMA_H
#define SISTEMA_H 1

#include <iostream>
#include <string>
#include "Dado.h"

using namespace std;

class Sistema {
    private:       
    Dado** instancias;
    Dado** instanciasRandomizadas;
    int tam;
    int tamRandomizadas;
    double metrica[2]; // 0 qntComp 1 qntkey
    ofstream* arqSaida;
    ofstream* arqSaidaModuloTeste;
    void merge(Dado **vec, Dado **aux, int inicio, int meio, int fim);
    void mergeSortAux(Dado **vec, Dado **aux, int inicio, int fim);
    void quickSortAux(Dado **vec, int inicio, int fim, int* qntComp, int* qntKey);
    int particionamento(Dado **vec, int inicio, int fim, int* qntComp, int* qntKey);
    void mergeCasos(Dado **vec, Dado **aux, int inicio, int meio, int fim, int *comp, int *mov);
    void mergeSortAuxRand(Dado **vec, Dado **aux, int inicio, int fim, int *comp, int *mov);
    void imprimirRandomizado();
    void gerarArquivoSaidaModuloTeste(string nomeAlgoritmo);
    void excluirRandomizada();
    void escreveArquivoSaida();
    void TransformaAcumuladoDiario();
    void lerAquivo(string nomeArq);    

    public:      
    Sistema(int x);
    ~Sistema();
    void mergeSort();
    void leituraRandomizada(int N);
    void shellSort();   
    void quickSort();
    void mergeSortRand();    
    void testarAlgoOrdenacao();
    void testarAlgoOrdenacaoModuloTeste();
    void ProcessarModuloTeste(string entrada);

};

#endif // SISTEMA_H
